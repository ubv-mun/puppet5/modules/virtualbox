# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include virtualbox::install
class virtualbox::install {
  package { $virtualbox::package_name:
    ensure => $virtualbox::package_ensure,
  }
}
